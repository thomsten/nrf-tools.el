;; nrf-tools.el -- nRF development with Emacs
;;
;; Author: Thomas Stenersen <stenersen.thomas@gmail.com>
;; Copyright (C) Thomas Stenersen, all rights reserved.
;;
;; Prerequisites:
;; - pynrfjprog (https://github.com/NordicSemiconductor/pynrfjprog, `pip install pynrfjprog')
;; - jlink-gdb.el (https://github.com/thomasstenersen/jlink-gdb.el)
;; - projectile
;; - thingatpt

(require 's)
(require 'python)
(require 'jlink-gdb)
(require 'thingatpt)
(require 'projectile)

(defgroup nrf-tools nil
  "nRF Tools customization group")

(setq nrf-tools-dir (file-name-directory (or load-file-name buffer-file-name)))

(defconst nrf-tools-script (concat nrf-tools-dir "nrf_tools.py")
  "Python script nRF connectivity.")

(defconst nrf-tools-buffer-regex
  "\\*\\([0-9A-Za-z._\\-]+\\)\\.hex:\\([0-9]\\{9\\}\\)\\*"
  "Regular expression to match (hexfile):(segger-id)")

(defcustom
  nrf-softdevice-s110 (concat nrf-tools-dir "s110/hex/s110_softdevice.hex")
  "Path to S110 SoftDevice"
  :type 'string
  :group 'nrf-tools)

(defcustom
  nrf-softdevice-s130 (concat nrf-tools-dir "s130/hex/s130_nrf51_2.0.1_softdevice.hex")
  "Path to S130 SoftDevice"
  :type 'string
  :group 'nrf-tools)

(defcustom
  nrf-softdevice-s132 (concat nrf-tools-dir "s132/hex/s132_nrf52_3.0.0_softdevice.hex")
  "Path to S132 SoftDevice"
  :type 'string
  :group 'nrf-tools)

(defcustom
  nrf-softdevice-default nrf-softdevice-s130
  "Default SoftDevice to use")

(defconst nrf-error-code-list
  '("NRF_SUCCESS"
    "NRF_ERROR_SVC_HANDLER_MISSING"
    "NRF_ERROR_SOFTDEVICE_NOT_ENABLED"
    "NRF_ERROR_INTERNAL"
    "NRF_ERROR_NO_MEM"
    "NRF_ERROR_NOT_FOUND"
    "NRF_ERROR_NOT_SUPPORTED"
    "NRF_ERROR_INVALID_PARAM"
    "NRF_ERROR_INVALID_STATE"
    "NRF_ERROR_INVALID_LENGTH"
    "NRF_ERROR_INVALID_FLAGS"
    "NRF_ERROR_INVALID_DATA"
    "NRF_ERROR_DATA_SIZE"
    "NRF_ERROR_TIMEOUT"
    "NRF_ERROR_NULL"
    "NRF_ERROR_FORBIDDEN"
    "NRF_ERROR_INVALID_ADDR"
    "NRF_ERROR_BUSY")
  "nRF error code look up table")

(defun nrf-error-code-to-string (error-code)
  "Gets the error code name from the given number."
  (interactive "nError code number: ")
  (message (nth error-code nrf-error-code-list)))

(defun nrf--connect (segger-id &optional hexfile)
  (if (not hexfile)
      (setq hexfile "unknown.hex"))
  (let* ((buffer-base-name (concat "unknown.hex:" segger-id))
        (buffer-name (concat "*" buffer-base-name "*")))
    (make-comint buffer-base-name python-shell-interpreter nil "-u" nrf-tools-script "connect" "-d" segger-id)))

(defun nrf-connect (&optional segger-id)
  "Connects to a given device on RTT"
  (interactive)
  (if segger-id
      (switch-to-buffer (nrf--connect segger-id))
    (switch-to-buffer
     (nrf--connect (completing-read "Which device do you want to connect to?" (nrf-get-device-list))))))

(defun nrf-program-and-connect (&optional no-softdevice)
  "Programs a device a device with a hex file interactively.

Set NO-SOFTDEVICE `t' to flash without a softdevice."
  (interactive)
  (if no-softdevice
      (setq softdevice nil)
    (setq softdevice nrf-softdevice-default))
  (let ((hexfile (read-file-name "Hex file: " nil nil nil nil nil))
        (segger-id (completing-read "Segger ID: " (nrf-get-device-list) nil t)))
    (switch-to-buffer (nrf--program-and-connect hexfile segger-id softdevice))))

(defun nrf--program-and-connect (hexfile segger-id &optional softdevice)
  (let* ((buffer-base-name (concat (file-name-nondirectory hexfile) ":" segger-id))
         (buffer-name (concat "*" buffer-base-name "*"))
         (comint-args (list buffer-base-name
                            python-shell-interpreter nil
                            "-u" nrf-tools-script
                            "program" "-d" segger-id "-f" hexfile "-c")))
    (if softdevice
        (setq comint-args (append comint-args (list "-s" softdevice))))
    (setq process-buffer
          (apply 'make-comint comint-args))
    ;; Set directory of buffer to be the same as the hex file (for jlink-gdb.el)
    (with-current-buffer process-buffer
      (setq default-directory (file-name-directory hexfile))
      (nrf-mode))
    process-buffer))

(defun nrf-program (hexfile segger-id &optional softdevice)
  (let* ((base-name-format "*nrf-tools-programming*")
         (program-buffer (get-buffer-create base-name-format))
         (process-args (list base-name-format
                             base-name-format
                             python-shell-interpreter "-u" nrf-tools-script
                             "program" "-d" segger-id "-f" hexfile "-r")))
    (if softdevice (append process-args '("-s" softdevice)))
    (apply 'start-process process-args)))

(defun nrf-multi-program (hexfile &optional softdevice)
  (mapc (lambda (segger-id) (nrf-program hexfile segger-id softdevice))
        (nrf-get-device-list)))

(defun nrf-get-device-list ()
  (split-string (shell-command-to-string (concat python-shell-interpreter " " nrf-tools-script " list"))))

(defun nrf-get-open-rtt-buffers ()
  "Gets a list of all open RTT buffers."
  (let ((buffer-regex nrf-tools-buffer-regex)
        (buffer-name-list (mapcar 'buffer-name (buffer-list))))
    (remove 'nil (mapcar (lambda (str) (first (s-match buffer-regex str))) buffer-name-list))))

(defun nrf-is-rtt-buffer-p (&optional a-buffer-name)
  (if a-buffer-name
      (s-matches? nrf-tools-buffer-regex a-buffer-name)
    (s-matches? nrf-tools-buffer-regex (buffer-name))))

(defun nrf-kill-rtt-buffer (buffer-or-name)
  "Sends a SIGKILL to the RTT buffer process and kills the buffer 500ms later."
  (let* ((the-buffer (get-buffer buffer-or-name))
         (the-buffer-process (get-buffer-process the-buffer)))
    (if the-buffer-process
        (kill-process the-buffer-process))
    (run-with-timer 0.5 nil 'kill-buffer the-buffer)))

(defun nrf-display-buffers (buffer-list)
  (delete-other-windows)
  (setq it 0)
  (dolist (buf buffer-list)
    (if (= it 0)
        (switch-to-buffer buf)
      (progn
        (if (= it 1)
            (split-window-vertically)
          (split-window-horizontally))
        (other-window 1)
        (switch-to-buffer buf)))
    (setq it (+ it 1)))
  (balance-windows))

(defun nrf-open-rtt-buffers ()
  "Displays all active RTT buffers."
  (interactive)
  (if (setq rtt-buffers (nrf-get-open-rtt-buffers))
      (nrf-display-buffers rtt-buffers)))

(defun nrf-kill-open-rtt-buffers ()
  "Kills all the opened RTT buffers"
  (interactive)
  (mapc 'nrf-kill-rtt-buffer (nrf-get-open-rtt-buffers)))

(defun nrf-get-devices ()
  (interactive)
  (let ((devices (nrf-get-device-list)))
    (message "Connected devices (%d): %s" (length devices) (s-join " " devices))))

(defun nrf--reset-device (segger-id)
  (if (s-matches-p "[0-9]\\{9\\}" segger-id)
      (progn (start-process (concat "*reset-segger-" segger-id "*")
                            nil
                            python-shell-interpreter
                            nrf-tools-script "reset" "-d" segger-id)
             (message "Reset device %s" segger-id))
    (message "Segger ID `%s' is not a valid segger ID." segger-id)))

(defun nrf-reset-device (&optional segger-id)
  "Interactively resets a device.

The device reset is either
- The given SEGGER-ID
- The device corresponding to the current buffer (if any), or
- One of the prompted devices"
  (interactive)
  (cond
   (segger-id
    (nrf--reset-device segger-id))
   ((nrf-is-rtt-buffer-p)
    (setq segger-id (third (s-match nrf-tools-buffer-regex (buffer-name))))
    (if (yes-or-no-p (format "Reset device %s?" segger-id))
        (nrf--reset-device segger-id)))
   (t
    (nrf--reset-device (completing-read "Device to reset: " (nrf-get-device-list) nil t)))))

(defun nrf-reset-all-devices ()
  "Reset all connected devices"
  (interactive)
  (if (yes-or-no-p "Reset all devices?")
      (mapc 'nrf--reset-device (nrf-get-device-list))))

(defun nrf--start-gdb-for-buffer (the-buffer-name)
  "Starts a JLink GDB session for the given buffer.
Assumes that the `default-directory' is set to the directory where the '.elf'-file of the process can be found.
Furthermore, it is assumed that the buffer is named on the form: *<program-name>.hex:<segger-id>*"
  (if (not (s-match nrf-tools-buffer-regex the-buffer-name))
      (error (format "Only buffers matching the regex %s may be used with this function" nrf-tools-buffer-regex))
    (let* ((re-matches (s-match nrf-tools-buffer-regex the-buffer-name))
           (base-file-name (nth 1 re-matches))
           (base-file-dir default-directory)
           (segger-id (nth 2 re-matches))
           (elf-file (cond
                      ((file-exists-p (concat base-file-dir base-file-name ".elf"))
                       (concat base-file-dir base-file-name ".elf"))
                      ((file-exists-p (concat base-file-dir base-file-name ".out"))
                       (concat base-file-dir base-file-name ".out"))
                      (t
                       (completing-read "Load symbol file: " nil)))))
      (jlink-gdb-start-gdb elf-file segger-id))))

(defun nrf--get-elf-files-in-project ()
  (let* ((build-dir (concat (projectile-project-root) "build")))
    (directory-files-recursively build-dir ".*\.elf")))

(defun nrf--erase-sentinel (process event)
  "Sentinel for an erase process."
  (message "Successfully erased: %s" (second
                                      (s-match "\\*nrfjprog-erase-\\([0-9]\\{9\\}\\)\\*"
                                               (process-name process)))))

(defun nrf-erase-device (segger-id)
  "Erases the given device"
  (interactive
   (list (completing-read "SEGGER ID: " (nrf-get-device-list))))
  (set-process-sentinel
   (start-process (format "*nrfjprog-erase-%s*" segger-id) nil "nrfjprog" "-e" "-s" segger-id)
   'nrf--erase-sentinel))

(defun nrf-erase-devices (device-list)
  "Erases all devices in DEVICE-LIST."
  (mapc 'nrf-erase-device device-list))

(defun nrf-erase-all-devices ()
  "Erases all connected devices."
  (interactive)
  (mapc 'nrf-erase-device (nrf-get-device-list)))

(defvar segger-hist '())
(defvar elf-hist '())
(defun nrf--start-gdb-for-buffer-interactively ()
  (interactive)
  (let* ((elf-file (completing-read ".elf file: " (nrf--get-elf-files-in-project) nil 'confirm nil 'elf-hist))
         (segger-id (completing-read "Segger ID: " (nrf-get-device-list) nil nil nil 'segger-hist)))
    (jlink-gdb-start-gdb elf-file segger-id)))

(defun nrf-start-gdb-for-current-buffer (&optional the-buffer-name)
  "Starts a JLink GDB session for the current active buffer."
  (interactive)
  (if (not the-buffer-name)
      (setq the-buffer-name (buffer-name)))
  (if (s-match nrf-tools-buffer-regex the-buffer-name)
      (nrf--start-gdb-for-buffer the-buffer-name)
    (nrf--start-gdb-for-buffer-interactively)))

(defvar nrf--prev-elf-file)
(defun nrf-addr2line-at-point (&optional elf-file address)
  "Find the file and line for a given address at point.
Useful in the case of an assertion on the form 'ASSERT at 0x00020492'."
  (interactive)
  (if (not elf-file)
      (if (setq re-matches (s-match nrf-tools-buffer-regex (buffer-name)))
          (progn
            (setq base-file-name (nth 1 re-matches))
            (setq base-file-dir default-directory)
            (setq segger-id (nth 2 re-matches))
            (setq elf-file (concat base-file-dir base-file-name ".elf")))
        (setq elf-file (read-file-name "What elf file?" default-directory nil t  nrf--prev-elf-file
                                       (lambda (name)
                                         (or (s-match "\\.elf$" name)
                                             (file-directory-p name)))))
        ))
  (setq nrf--prev-elf-file elf-file)
  (if (not address)
      (setq address (thing-at-point 'word)))

  (let ((file-and-line (second
                        (split-string
                         (shell-command-to-string
                          (concat
                           "addr2line -a " address " -e " elf-file))))))
    (if (s-matches-p ".*\.\\(c\\|h\\|cpp\\|hpp\\|cxx\\):[0-9]+" file-and-line)
        (progn
          (setq file-and-line (s-replace "\\" "/" file-and-line))
          (setq list (split-string file-and-line ":")) ; Windows compability (C:/...)
          (setq file (s-join ":" (subseq list 0 (1- (length list)))))
          (setq line (first (last list)))
          (find-file file)
          (goto-line (point-min))
          (forward-line (1- (string-to-int line))))
      (message "Could not find file:line for %s (%s)." address file-and-line))))

(define-minor-mode nrf-mode
  "Minor mode for interacting with nRF5x devices"
  :lighter "nRF"
  :keymap (let ((map (make-sparse-keymap)))
            (define-key map (kbd "C-c n l") 'nrf-get-devices)
            (define-key map (kbd "C-c n g") 'nrf-start-gdb-for-current-buffer)
            (define-key map (kbd "C-c n q") 'nrf-kill-open-rtt-buffers)
            (define-key map (kbd "C-c n o") 'nrf-open-rtt-buffers)
            (define-key map (kbd "C-c n r") 'nrf-reset-device)
            (define-key map (kbd "C-c n a") 'nrf-addr2line-at-point)
            map))

(provide 'nrf-tools)
