# nrf-tools.el -- Emacs package for working with nRF devices

## Features
- List connected devices `nrf-get-devices` or `nrf-get-device-list => '()`
- Program and connect to devices over RTT `nrf-program-and-connect`, only connect with `nrf-connect`
- Program a single device with `nrf-progam` and multiple in parallel with `nrf-multi-program`
- Reset connected devices interactively with `nrf-reset-device` and all devices with `nrf-reset-all-devices`
- Quick lookup of `NRF_ERROR_*` codes

## Dependencies
- `pynrfjprog` (`pip install pynrfjprog`)
- `python`
- [jlink-gdb.el](https://github.com/thomasstenersen/jlink-gdb.el)
- `projectile`
- `thingatpt`


## Example usage

```elisp
;; Program multiple devices
(setq hexfile "my_hex_file.hex")
(mapc (lambda (segger-id)
        (nrf--program-and-connect hexfile segger-id nrf-softdevice-default))
      (nrf-get-device-list))

(nrf-open-rtt-buffers) ; Display the RTT buffers
```
