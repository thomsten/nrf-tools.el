#!/usr/bin/env python3

import sys
import time
import threading
import logging
import argparse

try:
    from pynrfjprog.API import API as NrfAPI
    from pynrfjprog.API import DeviceFamily as NrfDeviceFamily
    import pynrfjprog.API as API
    from pynrfjprog.Hex import Hex
except ImportError:
    print("Error: Could not find pynrfjprog.\nHave you run `pip install pynrfjprog`?")
    exit(1)


def connect(snr=None, jlink_khz=50000):
    nrf = NrfAPI(NrfDeviceFamily.NRF52)
    nrf.open()
    if snr:
        nrf.connect_to_emu_with_snr(snr, jlink_khz)
    else:
        nrf.connect_to_emu_without_snr(jlink_khz)
    try:
        device_version = nrf.read_device_version()
    except API.APIError as e:
        if e.err_code == API.NrfjprogdllErr.WRONG_FAMILY_FOR_DEVICE:
            nrf.close()
            nrf = NrfAPI(NrfDeviceFamily.NRF52)
            nrf.open()
            if snr:
                nrf.connect_to_emu_with_snr(snr, jlink_khz)
            else:
                nrf.connect_to_emu_without_snr(jlink_khz)
        else:
            raise e
    return nrf

def list_devices():
    nrf = NrfAPI(NrfDeviceFamily.NRF51)
    nrf.open()
    devices = nrf.enum_emu_snr()
    if devices:
        print("\n".join(list(map(str, devices))))
    nrf.close()


class RTT(object):
    """RTT commication class"""
    def __init__(self, nrf):
        self._nrf = nrf
        self._close_event = None
        self._writer_thread = None
        self._reader_thread = None

    def _writer(self):
        while not self._close_event.is_set():
            data = sys.stdin.readline().strip("\n")
            if len(data) > 0:
                self._nrf.rtt_write(0, data)
                # Yield
            time.sleep(0.1)

    def _reader(self):
        BLOCK_SIZE = 512
        rtt_data = ""
        while not self._close_event.is_set():
            try:
                rtt_data = self._nrf.rtt_read(0, BLOCK_SIZE)
            except Exception as e:
                continue

            if rtt_data == "" or type(rtt_data) == int:
                time.sleep(0.1)
                continue
            rtt_data = rtt_data.rstrip("\r\n")
            for s in rtt_data.splitlines():
                if s.strip() == "":
                    continue
                try:
                    sys.stdout.buffer.write(bytes(s, "ascii"))
                except Exception as e:
                    continue

                sys.stdout.buffer.write(b'\n')
                sys.stdout.buffer.flush()

    def run(self):
        self._nrf.rtt_start()

        # Wait for RTT to find control block etc.
        time.sleep(0.5)
        try:
            while not self._nrf.rtt_is_control_block_found():
                logging.info("Looking for RTT control block...")
                self._nrf.rtt_stop()
                time.sleep(0.5)
                self._nrf.rtt_start()
                time.sleep(0.5)
        except KeyboardInterrupt as e:
            return

        self._close_event = threading.Event()
        self._close_event.clear()
        self._reader_thread = threading.Thread(target=self._reader)
        self._reader_thread.start()
        self._writer_thread = threading.Thread(target=self._writer)
        self._writer_thread.start()

        try:
            while self._reader_thread.is_alive() or \
                  self._writer_thread.is_alive():
                time.sleep(0.1)
        except KeyboardInterrupt as e:
            self._close_event.set()
            self._reader_thread.join()
            self._writer_thread.join()


class nRF(object):
    """nRF interaction class"""
    JLINK_KHZ = 50000

    def __init__(self, args):
        self.args = args
        if hasattr(args, 'jlink_khz'):
            self._jlink_khz = args.jlink_khz if args.jlink_khz else nRF.JLINK_KHZ
        else:
            self._jlink_khz = nRF.JLINK_KHZ
        self._snr = args.device_id
        self._nrf = connect(self._snr, self._jlink_khz)
        self._rtt = RTT(self._nrf)

    def _on_exit(self):
        logging.info("Exiting...")
        self._nrf.close()

    def flash(self):
        logging.info("Erasing {}".format(self._snr))
        self._nrf.erase_all()
        if self.args.softdevice:
            hexfiles = [self.args.softdevice, self.args.file]
        else:
            hexfiles = [self.args.file]

        for path in hexfiles:
            logging.info("Programming: \"{}\"".format(path))
            program = Hex(path)
            for segment in program:
                self._nrf.write(segment.address, segment.data, True)

    def reset(self):
        logging.info("Resetting device: {}".format(self._snr))
        self._nrf.sys_reset()
        self._nrf.go()

    def _do_program(self):
        self.flash()
        if self.args.reset or (self.args.file and self.args.connect):
            self.reset()
        if self.args.connect:
            self._rtt.run()

    def _do_connect(self):
        if self.args.reset:
            self.reset()
        self._rtt.run()

    def run(self):
        if self.args.command == 'program':
            self._do_program()
        elif self.args.command == 'connect':
            self._do_connect()
        elif self.args.command == 'reset':
            self.reset()
        else:
            raise("Error unknown command {}".format(self.args))
        self._on_exit()


class CLI(object):
    def __init__(self):
        self.parser = argparse.ArgumentParser(
            description="nRF Tools -- nRF interaction script")
        self.subparsers = self.parser.add_subparsers(dest="command")
        self.args = None

        self._add_program_command()
        self._add_connect_command()
        self._add_list_command()
        self._add_reset_command()

    def _add_program_command(self):
        program_parser = self.subparsers.add_parser("program")
        self._add_device_argument(program_parser)
        self._add_file_argument(program_parser)
        self._add_softdevice_argument(program_parser)
        self._add_jlink_khz_argument(program_parser)
        self._add_reset_argument(program_parser)
        self._add_connect_argument(program_parser)

    def _add_connect_command(self):
        connect_parser = self.subparsers.add_parser("connect")
        self._add_device_argument(connect_parser)
        self._add_reset_argument(connect_parser)
        self._add_jlink_khz_argument(connect_parser)

    def _add_reset_command(self):
        reset_parser = self.subparsers.add_parser("reset", help="Reset device")
        self._add_device_argument(reset_parser)

    def _add_list_command(self):
        self.subparsers.add_parser("list", help="List connected devices")

    def _add_device_argument(self, parser):
        parser.add_argument("-d", "--device-id",
                            type=int,
                            help="Segger ID of device.")

    def _add_file_argument(self, parser):
        parser.add_argument("-f", "--file",
                            type=str,
                            help="Path to firmware to flash.")

    def _add_softdevice_argument(self, parser):
        parser.add_argument("-s", "--softdevice",
                            type=str,
                            help="Path to S13X SoftDevice. " +
                            "If set, the specified SoftDevice will be " +
                            "merged with the given firmware (optional).",
                            default="")

    def _add_jlink_khz_argument(self, parser):
        parser.add_argument("-j", "--jlink-khz",
                            type=int,
                            help="Speed of JLink connection " +
                            "(max/default: 50000)",
                            default=50000)

    def _add_reset_argument(self, parser):
        parser.add_argument("-r", "--reset",
                            help="Reset device on init?",
                            action="store_true")

    def _add_connect_argument(self, parser):
        parser.add_argument("-c", "--connect",
                            help="Connect to device on RTT?",
                            action="store_true")

    def run(self):
        self.args = self.parser.parse_args()
        if self.args.command == 'program' and not self.args.file:
            self.parser.error("A file is required for programming the device")


def main():
    logging.basicConfig(format='%(message)s', level=logging.INFO)
    cli = CLI()
    cli.run()
    if cli.args.command == "list":
        list_devices()
    else:
        nrf = nRF(cli.args)
        nrf.run()


if __name__ == "__main__":
    main()
